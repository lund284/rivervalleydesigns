<?php
	
$a = array(
	'' => get_field(''),
	'' => get_field(''),
	'' => get_field('')
);
$img = get_field('');
$imgUrl = $img['url'];
$imgAlt = $img['alt'];
$imgTitle = $img['title'];
/* 
	'' => get_field(''),
	
	<?php echo $['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>
<?php				
	if (have_posts()) {
		while (have_posts()) {
			the_post(); // initialize $post variable and the_ functions like the_title()
?>
				<section class="wysiwyg-content">
					<div class="row">
						<div class="large-4 columns">
							<h4>Drop us a message</h4>
								<div class="phone-email">
								<a href="tel:<?php echo get_field('phone_number', 'options'); ?>" class="phone-link"><?php echo get_field('phone_number', 'options'); ?></a>
								<a href="mailto:<?php echo get_field('email_address', 'options'); ?>" class="email-link"><?php echo get_field('email_address', 'options'); ?></a>						
								</div>
						</div>
						<div class="large-8 columns">
							<?php if (!empty(get_the_content())) {
							 	the_content(); 
							 } ?>
						</div>
					</div>
				</section>
<?php
		}
	} 
?>
