<?php
	
$a = array(
	'' => get_field(''),
	'' => get_field(''),
	'' => get_field('')
);
$img = get_field('');
$imgUrl = $img['url'];
$imgAlt = $img['alt'];
$imgTitle = $img['title'];
/* 
	'' => get_field(''),
	
	<?php echo $['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>
<?php				
	if (have_posts()) {
		while (have_posts()) {
			the_post(); // initialize $post variable and the_ functions like the_title()
?>
				<section class="wysiwyg-content">
					<div class="row">
						<?php if (!empty(get_the_content())) {
						 	the_content(); 
						 } ?>
					</div>
				</section>
<?php
		}
	} 
?>
