<?php
	
$innerhero = array(
	'text' => get_field('ih_text'),
	'image' => get_field('ih_image')
);
$img = get_field('');
$imgUrl = $img['url'];
$imgAlt = $img['alt'];
$imgTitle = $img['title'];
/* 
	'' => get_field(''),
	
	<?php echo $['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="inner-hero"  style="background-image: url(<?php echo $innerhero['image']; ?>);">
	<div class="row">
		<div class="overlay"></div>
		<div class="box">
			<h2><?php echo $innerhero['text']; ?></h2>
		</div>
	</div>
</section>

