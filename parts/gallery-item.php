<?php
// Get list of project categories
$project_categories = get_the_terms( get_the_ID(), 'project_cats' );
// Get from term object only category title
$project_categories_list = array_map( function ( $v ) {
	return sanitize_title( $v->name );
}, $project_categories );

$project_cat = array_map( function ( $v ) {
	return $v->name ;
}, $project_categories );

$gal_modal = get_field('gallery_modal_text', 'options');

$next_post = get_next_post();
$next_post_id = $next_post->ID;

$previous_post = get_previous_post();
$previous_post_id = $previous_post->ID;

$mod_category = get_the_category();
$bldg_num = get_field('bldg_number');

$catWordSelector = get_field('mc_select');
$catCustomHdr = get_field('mc_custom_header');

?>
<div class="large-4 small-6 columns gallery-item-wrap end <?php echo implode( ' ', $project_categories_list ); ?>">
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'gallery-item' ); ?>>
		<a data-open="a<?php the_ID(); ?>"><?php the_post_thumbnail( 'medium_large', array( 'class' => 'gallery-item__img' ) ); ?></a>
		<div class="gallery-item__hover">
			<div class="gallery-item__link" data-toggle="">
				<a data-open="a<?php the_ID(); ?>"><!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/view.png" /> --></a>
			</div>
		</div>
	</article>
</div>


<div class="reveal large row-inner project-modal" id="a<?php the_ID(); ?>" data-reveal data-toggle="">
	<p class="close-button" data-close>x</p>
	<div class="row column modal-hdr">
		<h5><?php the_title(); ?></h5>
<!-- 		<p class="cat-title"><span><?php echo implode(",</span> <span>", $project_cat); ?></span></p> -->
	</div>
	<hr>
	<div class="row modalcontent">
		<div class="small-8 columns img">
		<?php  //the_post_thumbnail( 'medium_large', array( 'class' => 'gallery-item__img' ) ); ?>
		
		
		
<?php 
$images = get_field('project_gallery_photos');
if( !$images ): 
 the_post_thumbnail( 'medium_large', array( 'class' => 'gallery-item__img' ) ); 
endif;
if( $images ): ?>
    <div id="slider" class="modal-gallery">
<!--         <div class="slides"> -->
            <?php foreach( $images as $image ): ?>
                <div class="modal-slides">
                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <p><?php echo esc_html($image['caption']); ?></p>
                </div>
            <?php endforeach; ?>
<!--         </div> -->
    </div>
<!--
    <div id="carousel" class="modal-nav">
        <ul class="slides modal-gal-nav">
            <?php foreach( $images as $image ): ?>
                <li class="slide">
                    <img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="Thumbnail of <?php echo esc_url($image['alt']); ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
-->
    <?php endif; ?>
	
	
	
	
		</div>
		<div class="small-4 columns content">
		<?php		
		if (!empty(get_the_content())) {	
			?>
			<h4><?php echo get_field('mc_custom_header');?></h4>
			<?php the_content();
  
		} else {
			
		if( have_rows('modal_content', 'options') ):
			    while ( have_rows('modal_content', 'options') ) : the_row();
			    $mcSelector = get_sub_field('selector');
			     if($catWordSelector == $mcSelector) {
				?>    
					<h4><?php the_sub_field('gm_header');?></h4>
			        <?php the_sub_field('gm_content');?>
			<?php    
				} 
			    endwhile;
			else :
			endif;
				
			?>
		<?php	 } ?>
			
		</div>
	</div>
	<hr>
	<div class="modal-ftr row">
		<div class="small-6 columns mod-cta">
			<h6>Ready to Build Something Like This?</h6>
		</div>
		<div class="small-6 columns">
			<a href="<?php echo get_home_url(); ?>/contact/">Contact us here »</a>
		</div>

<!--
		<div class="small-2 columns mod-nav">
			<?php if (!empty($previous_post_id)) {?>
			<a data-open="a<?php echo $previous_post_id; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/modal-previous.png" /></a>
			<?php } ?>
			<?php if (!empty($next_post_id)) {?>
			<a data-open="a<?php echo $next_post_id; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/modal-next.png" /></a>
			<?php	 } ?>
		</div>
-->
	</div>
</div>
<!--
<script>
/*
	        jQuery(window).trigger('resize');

*/
    jQuery('.reveal-overlay').on('open', function() {


	var something = jQuery(".overlay")
    .css({backgroundColor: 'white'})
    .show()        
setTimeout(function(){
    something.css({backgroundColor: ''});
},6000);
	    });

</script>
-->