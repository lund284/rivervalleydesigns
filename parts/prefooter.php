<?php
$preftr_cta_to = array(
	'headingop' => get_field('pfcta_heading', 'options'),
	'contentop' => get_field('pfcta_content', 'options'),
	'btn_textop' => get_field('pfcta_button_text', 'options'),
	'btn_linkop' => get_field('pfcta_button_link', 'options')
);	
$preftr_cta = array(
	'heading' => get_field('pfcta_heading'),
	'content' => get_field('pfcta_content'),
	'btn_text' => get_field('pfcta_button_text'),
	'btn_link' => get_field('pfcta_button_link')
);
$customize = get_field('pfcta_customize');
/* 
	'' => get_field(''),
	
	<?php echo $['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="preftr-cta">
	<div class="row">
		<?php 
		if (!empty($customize)) { 
		?>
		<h2><?php echo $preftr_cta['heading']; ?></h2>
		<h5><?php echo $preftr_cta['content']; ?></h5>
		<a href="<?php echo $preftr_cta['btn_link']; ?>" class="button navy-button"><?php echo $preftr_cta['btn_text']; ?></a>
		<?php } else { ?>
		<h2><?php echo $preftr_cta_to['headingop']; ?></h2>
		<h5><?php echo $preftr_cta_to['contentop']; ?></h5>
		<a href="<?php echo $preftr_cta_to['btn_linkop']; ?>" class="button navy-button"><?php echo $preftr_cta_to['btn_textop']; ?></a>
		
		<?php } ?> 
	</div>
</section>

