<div id="stickyTopAnchor" data-sticky-container>
	<div class="top-bar sticky rufio-alt-nav" data-sticky data-margin-top="0" data-margin-bottom="0" data-top-anchor="stickyTopAnchor" data-btm-anchor="stickyBtmAnchor" data-options="stickyOn:small;">
		<div class="row">
			
			<div class="show-for-small hide-for-large mobile-menu">
				<nav class="column small-4 float-left logo">
					<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/RVDLogo.png'; ?>" alt="<?php bloginfo('name'); ?>">
					</a>
				</nav>
				<nav class="column small-12 mmenu">
					<button id="mainNavBtn" class="float-right hamburger hamburger-collapse" data-toggle="navmenu"><p><span class="hamburger-box"><span class="hamburger-inner"></span></span></p><i class="i-menu"></i></button>
					<div class="top-drop fast" id="navmenu" data-toggler data-animate="hinge-in-from-top hinge-out-from-top">
						<?php rufio_alt_t_nav(); ?>
					</div>
				</nav>
			</div>
			
			<div class="hide-for-small-only hide-for-medium-only desktop-menu row" data-equalizer="header">
				<nav class="column small-2 logo" data-equalizer-watch="header">
					<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/RVDLogo.png'; ?>" alt="<?php bloginfo('name'); ?>">
					</a>
				</nav>
				<nav class="column large-7 menu" data-equalizer-watch="header">
					<div class="">
						<?php rufio_t_nav(); ?>
					</div>
				</nav>
				<div class="column large-3 phone-email" data-equalizer-watch="header">
					<div class="vert-center">
						<a href="tel:<?php echo get_field('phone_number', 'options'); ?>" class="phone-link"><?php echo get_field('phone_number', 'options'); ?></a>
						<a href="mailto:<?php echo get_field('email_address', 'options'); ?>" class="email-link"><?php echo get_field('email_address', 'options'); ?></a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>