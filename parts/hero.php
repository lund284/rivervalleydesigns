<?php
	
$a = array(
	'' => get_field(''),
	'' => get_field(''),
	'' => get_field('')
);
/* 
	'' => get_field(''),
	
	<?php echo $['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="home-hero">
	<div class="row expanded">
		<div class="hh-slider" data-equalizer="homehero">
		<?php
			if( have_rows('home_hero_slider') ):
			    while ( have_rows('home_hero_slider') ) : the_row();
			    $buttonText = get_sub_field('button_text');
			?>    
			<div class="hh-slide" style="background-image: url(<?php the_sub_field('image');?>)" data-equalizer-watch="homehero">
				<div class="overlay"></div>
				<div class="con-con">
			       <h1><?php the_sub_field('header_text');?></h1>
			       <h4><?php the_sub_field('subheader');?></h4>
			       <?php if ( !empty($buttonText)) { ?>
			       <a href="<?php the_sub_field('button_action');?>" class="button teal-button"><?php the_sub_field('button_text');?></a>			       
			       <?php } ?>
				</div>

			</div>
			<?php     
			    endwhile;
			else :
			endif;
			?>	
		</div>
	</div>	
</section>

