<section class="gallery">

	<div class="row row-inner">
		<!-- BEGIN of Input search filter -->
<!--
		<div class="columns large-6">
			<div class="search-ideas">
				<div class="search-ideas__form">
					<input type="text" name="s" class="search-ideas__input" placeholder="<?php _e( 'Search ideas...', 'rock' ); ?>">
					<button class="button search-ideas__submit"><?php _e( 'Search', 'rock' ); ?></button>
				</div>
			</div>
		</div>
-->
		<!-- END of Input search filter -->
		
		<!-- BEGIN of categories filter -->
		<?php $project_categories = get_terms( array( 'taxonomy' => 'project_cats', 'hide_empty' => false, 'orderby' => 'menu_order' ) ); ?>
		<div class="columns filters">
			<?php if ( $project_categories ): ?>
				<ul class="gallery-filter">
					<?php foreach ( $project_categories as $project_category ): ?>
						<li class="gallery-filter__item">
							<button class="button navy-button" data-filter="<?php echo '.' . sanitize_title( $project_category->name ); ?>"><span><?php echo $project_category->name; ?></span></button>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
		<!-- END of categories filter -->
	</div>
	
	<!-- BEGIN of list of projects -->
	<div class="row columns row-inner grid">
		<div class="gallery-grid">
			<?php $projectsArgs = array(
				"post_type"      => "projects",
				"orderby"        => "date",
				"order"          => "DESC",
				"posts_per_page" => "-1"
			);
			$projects           = new WP_Query( $projectsArgs );
			if ( $projects->have_posts() ):
				while ( $projects->have_posts() ): $projects->the_post();
					get_template_part( 'parts/gallery-item' ); // Gallery item
				endwhile;
			endif;
			wp_reset_query(); ?>
		</div>
	</div>
	<!-- END of list of projects -->

</section>

