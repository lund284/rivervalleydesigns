<?php
	
$cta = array(
	'heading' => get_field('cta_heading'),
	'content' => get_field('cta_content'),
	'btn_text' => get_field('cta_button_text'),
	'btn_link' => get_field('cta_button_link')
);
/* 
	'' => get_field(''),
	
	<?php echo $cta['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="cta">
	<div class="row">
		<h2><?php echo $cta['heading']; ?></h2>
		<h5><?php echo $cta['content']; ?></h5>
		<a href="<?php echo $cta['btn_link']; ?>" class="button teal-button"><?php echo $cta['btn_text']; ?></a>
	</div>
</section>

