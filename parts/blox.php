<?php
	
$slider_blocks = array(
	'title1' => get_field('hpsb1_title'),
	'subtitle1' => get_field('hpsb1_sub_title'),
	'title2' => get_field('hpsb2_title'),
	'subtitle2' => get_field('hpsb2_sub_title'),
	'title3' => get_field('hpsb3_title'),
	'subtitle3' => get_field('hpsb3_sub_title'),
	'title4' => get_field('hpsb4_title'),
	'subtitle4' => get_field('hpsb4_sub_title')
);
$count1 = 0;
$img1 = get_field('hpsb1_images');
if (is_array($img1)) {
	$count1= count($img1);
}
$count2 = 0;
$img2 = get_field('hpsb2_images');
if (is_array($img2)) {
	$count2 = count($img2);
}
$count3 = 0;
$img3 = get_field('hpsb3_images');
if (is_array($img3)) {
	$count3 = count($img3);
}
$count4 = 0;
$img4 = get_field('hpsb4_images');
if (is_array($img4)) {
	$count4 = count($img4);
}	
/* 
	'' => get_field(''),
	
	<?php echo $slider_blocks['']; ?>
	
	<?php
	if ( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="service-blocks" data-equalizer="slider-blocks" data-equalize-on-stack="false">
	<div class="row expanded">
	<?php
		if ($count1 >= 2 ) {
	?>
		<div class="hp-slider-block medium-6 columns">
	<?php } else { ?>
		<div class="hp-block medium-6 columns">
		<?php
			}
			if ( have_rows('hpsb1_images') ):
			    while ( have_rows('hpsb1_images') ) : the_row();
			    $img1bg = get_sub_field('sb1_image');
				?>   		
				<div class="ser-block" style="background-image: url(<?php echo $img1bg;?>);" data-equalizer-watch="slider-blocks">
					<div class="textcontain">
					<h2><?php echo $slider_blocks['title1']; ?></h2>
					<p><?php echo $slider_blocks['subtitle1']; ?></p>
					</div>
				</div>
			<?php     
			    endwhile;
			else :
			endif;
			?>	
		</div>
		
			<?php
		if ($count2 >= 2 ) {
	?>
		<div class="hp-slider-block medium-6 columns">
	<?php } else { ?>
		<div class="hp-block medium-6 columns">
		<?php
			}
			if ( have_rows('hpsb2_images') ):
			    while ( have_rows('hpsb2_images') ) : the_row();
			    $img2bg = get_sub_field('sb2_image');
				?>   		
				<div class="ser-block" style="background-image: url(<?php echo $img2bg;?>);" data-equalizer-watch="slider-blocks">
					<div class="textcontain">
					<h2><?php echo $slider_blocks['title2']; ?></h2>
					<p><?php echo $slider_blocks['subtitle2']; ?></p>
					</div>
				</div>
			<?php     
			    endwhile;
			else :
			endif;
			?>	
		</div>
		
			<?php
		if ($count3 >= 2 ) {
	?>
		<div class="hp-slider-block medium-6 columns">
	<?php } else { ?>
		<div class="hp-block medium-6 columns">
		<?php
			}
			if ( have_rows('hpsb3_images') ):
			    while ( have_rows('hpsb3_images') ) : the_row();
			    $img3bg = get_sub_field('sb3_image');
				?>   		
				<div class="ser-block" style="background-image: url(<?php echo $img3bg;?>);" data-equalizer-watch="slider-blocks">
					<div class="textcontain">
					<h2><?php echo $slider_blocks['title3']; ?></h2>
					<p><?php echo $slider_blocks['subtitle3']; ?></p>
					</div>
				</div>
			<?php     
			    endwhile;
			else :
			endif;
			?>	
		</div>
		
			<?php
		if ($count4 >= 2 ) {
	?>
		<div class="hp-slider-block medium-6 columns">
	<?php } else { ?>
		<div class="hp-block medium-6 columns">
		<?php
			}
			if ( have_rows('hpsb4_images') ):
			    while ( have_rows('hpsb4_images') ) : the_row();
			    $img4bg = get_sub_field('sb4_image');
				?>   		
				<div class="ser-block" style="background-image: url(<?php echo $img4bg;?>);" data-equalizer-watch="slider-blocks">
					<div class="textcontain">
					<h2><?php echo $slider_blocks['title4']; ?></h2>
					<p><?php echo $slider_blocks['subtitle4']; ?></p>
					</div>
				</div>
			<?php     
			    endwhile;
			else :
			endif;
			?>	
		</div>
	</div>
</section>

