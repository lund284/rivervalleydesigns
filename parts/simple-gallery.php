<?php
	
$simple_gallery = array(
	'header' => get_field('sg_header'),
	'btn_text' => get_field('sg_button_text'),
	'btn_link' => get_field('sg_button_link'),
	'img1' => get_field('sg_image_1'),
	'img2' => get_field('sg_image_2'),
	'img3' => get_field('sg_image_3'),
	'img4' => get_field('sg_image_4'),
// 	'img5' => get_field('sg_image_5')
);
/* 
	'' => get_field(''),
	
	<?php echo $simple_gallery['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<!--
<section class="home-gallery">
	<div class="row heading">
		<h3><?php echo $simple_gallery['header']; ?></h3>
		<a href="<?php echo $simple_gallery['btn_link']; ?>" class="button"><?php echo $simple_gallery['btn_text']; ?></a>
	</div>
	<div class="row expanded images" data-equalizer="galtop">
		<div class="large-7 columns top-left" data-equalizer-watch="galtop">
			<img src="<?php echo $simple_gallery['img1']; ?>" />
		</div>
		<div class="large-5 columns top-right" data-equalizer-watch="galtop">
			<div class="large-12 columns trtop">
				<img src="<?php echo $simple_gallery['img2']; ?>" />
			</div>
			<div class="large-12 columns trbot">
				<img src="<?php echo $simple_gallery['img3']; ?>" />				
			</div>
		</div>
		<div class="large-5 columns">
			<img src="<?php echo $simple_gallery['img4']; ?>" />
		</div>
		<div class="large-7 columns">
			<img src="<?php echo $simple_gallery['img5']; ?>" />
		</div>
	</div>
</section>
-->

<section class="home-gallery">
	<div class="row expanded" data-equalizer="homegallery" data-equalize-on-stack="false">
		<div class="large-5 columns" data-equalizer-watch="homegallery">
			<div class="row heading">
				<div class="vert-center">
				<h3><?php echo $simple_gallery['header']; ?></h3>
				<a href="<?php echo $simple_gallery['btn_link']; ?>" class="button teal-button"><?php echo $simple_gallery['btn_text']; ?></a>
				</div>
			</div>
		</div>
		<div class="large-7 columns images" data-equalizer-watch="homegallery">
			<div class="row">
				<div class="medium-7 columns top-left imgbg" data-equalizer-watch="galtop" style="background-image: url(<?php echo $simple_gallery['img1']; ?>);">
				</div>
				<div class="medium-5 columns top-right imgbg" data-equalizer-watch="galtop">
<!--
					<div class="large-12 columns trtop imgbg" style="background-image: url(<?php echo $simple_gallery['img2']; ?>);">
					</div>
-->
					<div class="large-12 columns trbot imgbg" style="background-image: url(<?php echo $simple_gallery['img2']; ?>);">
					</div>
				</div>
				<div class="medium-5 columns bot-left imgbg" style="background-image: url(<?php echo $simple_gallery['img3']; ?>);">
				</div>
				<div class="medium-7 columns bot-right imgbg" style="background-image: url(<?php echo $simple_gallery['img4']; ?>);">
				</div>
			</div>
		</div>
	</div>
</section>

