<?php

/* 
	'' => get_field(''),
	
	<?php echo $footer_opt['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>
					<footer class="footer" role="contentinfo" id="stickyBtmAnchor"><?php // stickyBtmAnchor is required for the sticky top bar to work right ?>
						<div class="row" data-equalizer="footer" data-equalize-on-stack="false">
							<div class="large-4 medium-3 columns logo" data-equalizer-watch="footer">
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/RVDLogo.png'; ?>" alt="<?php bloginfo('name'); ?>">							
							</div>
							<div class="large-4 medium-3 columns social" data-equalizer-watch="footer">
								<?php
								if( have_rows('social_links', 'options') ):
								    while ( have_rows('social_links', 'options') ) : the_row();
								?>    
								        <a href="<?php the_sub_field('link');?>"><img class="social-ftr" src="<?php the_sub_field('icon');?>" /></a>
								<?php     
								    endwhile;
								else :
								endif;
								?>
							</div>
							<div class="large-4 medium-6 columns phone-email" data-equalizer-watch="footer">
								<a href="tel:<?php echo get_field('phone_number', 'options'); ?>" class="phone-link"><?php echo get_field('phone_number', 'options'); ?></a>
								<a href="mailto:<?php echo get_field('email_address', 'options'); ?>" class="email-link"><?php echo get_field('email_address', 'options'); ?></a>
							</div>
							
						</div> <!-- end #inner-footer -->
						<div class="row">
							<div class="copy">
								<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
							</div>
						</div>
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->