<?php // Place custom php function snippets in this file. 
	
	function cptui_register_my_cpts_projects() {

	/**
	 * Post Type: Projects.
	 */

	$labels = array(
		"name" => __( "Projects", "" ),
		"singular_name" => __( "Project", "" ),
		"menu_name" => __( "Projects", "" ),
		"all_items" => __( "Projects", "" ),
		"add_new_item" => __( "Add New Project", "" ),
		"edit_item" => __( "Edit Project", "" ),
		"new_item" => __( "New Project", "" ),
		"view_item" => __( "View Project", "" ),
		"view_items" => __( "View Projects", "" ),
		"search_items" => __( "Search Projects", "" ),
		"not_found" => __( "No Projects Found", "" ),
		"not_found_in_trash" => __( "No Projects Found In Trash", "" ),
		"parent_item_colon" => __( "Project", "" ),
		"featured_image" => __( "Featured Image", "" ),
		"set_featured_image" => __( "Set Featured Image", "" ),
		"remove_featured_image" => __( "Remove Featured Image", "" ),
		"use_featured_image" => __( "Use as featured image for this project", "" ),
		"parent_item_colon" => __( "Project", "" ),
	);

	$args = array(
		"label" => __( "Projects", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => "projects",
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "projects", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
	);

	register_post_type( "projects", $args );
}

add_action( 'init', 'cptui_register_my_cpts_projects' );


function cptui_register_my_taxes_project_cats() {

	/**
	 * Taxonomy: Project Categories.
	 */

	$labels = array(
		"name" => __( "Project Categories", "" ),
		"singular_name" => __( "Project Category", "" ),
	);

	$args = array(
		"label" => __( "Project Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Project Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'project_cats', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "project_cats",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "project_cats", array( "projects" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_project_cats' );

?>