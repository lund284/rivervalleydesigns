<?php /* Template Name: Gallery Page */

get_header(); ?>

	<div id="content" class="home-page">
		<?php
			get_template_part( 'parts/inner-hero' );
			get_template_part( 'parts/gallery' );
			get_template_part( 'parts/prefooter' );			
		?>
				
	</div> <!-- end #content -->
	<script>
		jQuery( window ).on( 'load', function ($) {
			$ = jQuery;
		// quick search regex
		    var qsRegex;
		// init Isotope
		    var $grid = $('.gallery-grid').isotope({
		        itemSelector: '.gallery-item-wrap',
		        filter: function() {
		            return qsRegex ? $(this).text().match( qsRegex ) : true;
		        }
		    });
		// use value of search field to filter
/*
		    var $quicksearch = $('.search-ideas__input').keyup( debounce( function() {
		        qsRegex = new RegExp( $quicksearch.val(), 'gi' );
		        // console.log(12312);
		        $grid.isotope({
		            filter: function() {
		                if($('.gallery-filter__item--active').length){
		                    var filterValue = $('.gallery-filter__item--active').find('.button').data('filter');
		                    return ($(this).text().match( qsRegex ) && $(this).hasClass(filterValue.slice(1)));
		                } else {
		                    return qsRegex ? $(this).text().match( qsRegex ) : true;
		                }
		            }
		        });
		    }, 200 ) );
*/
		    $('.gallery-filter__item').on('click', 'button', function () {
		        var $filterToggle = $(this);
		        if ($filterToggle.parent().hasClass('gallery-filter__item--active')) {
		            $filterToggle.parent().removeClass('gallery-filter__item--active');
		            if (!qsRegex) {
		                $grid.isotope({filter: '*'});
		                return;
		            }
		            $grid.isotope({
		                filter: function () {
		                    return ($(this).text().match(qsRegex));
		                }
		            });
		        } else {
		            $('.gallery-filter__item').removeClass('gallery-filter__item--active');
		            $filterToggle.parent().addClass('gallery-filter__item--active');
		            var filterValue = $filterToggle.attr('data-filter');
		            if (!qsRegex) {
		                $grid.isotope({filter: filterValue});
		                return;
		            }
		            $grid.isotope({
		                filter: function () {
		                    return ($(this).text().match(qsRegex) && $(this).hasClass(filterValue.slice(1)));
		                }
		            });
		        }
		    });		
		// debounce so filtering doesn't happen every millisecond
		    function debounce( fn, threshold ) {
		        var timeout;
		        return function debounced() {
		            if ( timeout ) {
		                clearTimeout( timeout );
		            }
		            function delayed() {
		                fn();
		                timeout = null;
		            }
		            timeout = setTimeout( delayed, threshold || 100 );
		        }
		    }
		});
	</script>
<?php get_footer(); ?>