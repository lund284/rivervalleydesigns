<?php /* Template Name: Contact Page */

get_header(); ?>

	<div id="content" class="default-page">
		<?php
			get_template_part( 'parts/inner-hero' );
			get_template_part( 'parts/contact-content' );
			//get_template_part( 'parts/prefooter' );			
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>