<?php /* Template Name: Home Page */

get_header(); ?>

	<div id="content" class="home-page">
		<?php
			get_template_part( 'parts/hero' );
			get_template_part( 'parts/content' );
			get_template_part( 'parts/blox' );
			get_template_part( 'parts/cta' );
			get_template_part( 'parts/simple-gallery' );
			get_template_part( 'parts/prefooter' );			
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>